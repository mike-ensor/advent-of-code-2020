# Overview

The code included in this repository is used to solve the [Advent of Code](https://adventofcode.com/) problems. The folder structure is setup to include a solution for each of the two daily produced problems.

## Running tests

### All Tests
```bash
go test ./...
```

### Day or Day+Task Test
For a certain day, change directory into that folder (day) or subfolders (specific day+task)
```bash
go test ./...
```

### Day / Task Run
```bash
go run main.go # assuming golang is always used(will revisit this decision later)
```


## Day / Tasks

I will provide a quick summary of the task, the solution and a reflection

### Day 1

### Reflection
The tasks are completed for the day, but I'm not super happy with my result. I went the super quick-and-dirty route and ended up with N^2 and N^3 approaches. If I have time today/tomorrow, I'm going to approach as a "tape"

1. Create 2 or 3 tapes (array) of the sequence representing first found number, second found number and third found number (tape 1, 2, 3)
1. Add all numbers for each tape's index (ie: `a[0]` + `b[0]` + `c[0]` = `sum0`, `a[1]` + `b[1]` + `c[1]` = `sum1`) if `sum` == target number (`2020`) then you have the numbers, else, move tape 2 over by 1 (ie: `a[0]` + `b[1]` + `c[0]`)
    * >NOTE: each iteration results in `n` sums, all need to be checked for the target
1. Repeat until tape 2 = end, then reset and start with tape 3

#### Task 1
[source day 1 - task 1](day-1/day-1-problem-1)
Looking for a 2 numbers in a sequence that add up to `2020`, then taking those two numbers and finding their product (multiplying by each other)


#### Task 2

[source day 1 - task 2](day-1/day-1-problem-2)
Extending the first task but looking for a sequence of 3.

#### Enhancements
