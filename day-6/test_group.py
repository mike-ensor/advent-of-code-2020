import unittest

from group import Group

class GroupTestCase(unittest.TestCase):

   def test_get_set_of_answers_simple(self):
      answers = ['x', 'x', 'x']
      group = Group(answers)
      setOfAnswers = group.getAnswers()
      self.assertEqual(len(setOfAnswers), 1)

   def test_get_set_of_answers_complex(self):
      answers = ['x', 'x', 'mdtxr']
      group = Group(answers)
      setOfAnswers = group.getAnswers()
      self.assertEqual(len(setOfAnswers), 5)

   def test_getcount(self):
      answers = ['x', 'x', 'mdtxr']
      group = Group(answers)
      self.assertEqual(group.getCountAnswers(), 5)

   def test_get_three_same_answers(self):
      answers = ['aa', 'bb', 'cc']
      group = Group(answers)
      setOfAnswers = group.getAnswers()
      self.assertEqual(len(setOfAnswers), 3)

   def test_get_participants_answers(self):
      answers = ['ab', 'cd', 'ef'] # 3 participants
      group = Group(answers)
      participantsAnswers = group.getAllParticipantsAnswers()
      self.assertEqual(len(participantsAnswers), 3)
