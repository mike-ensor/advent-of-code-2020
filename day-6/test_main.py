import unittest
import logging

from main import getGroups
from main import getLines
from main import getSumOfGroups
from main import getSumConcensusAnswers
class TestRun:
    def __init__(self, expected, answerGroup, message=""):
        self.expected = expected
        self.answerGroup = answerGroup
        self.message = message
    def getExpected(self):
        return self.expected
    def getAnswerGroup(self):
        return self.answerGroup
    def getMessage(self):
        return self.message

class MainTestCase(unittest.TestCase):

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    def test_main(self):
        result = getGroups(getLines("simple-test.file"))
        self.assertEqual(len(result), 5)

    def test_getGroupOfGroups(self):
        groupsOfAnsweredYes = getGroups(getLines("simple-test.file"))
        expectedGroupCount = 5

        groupTwoParticpantCount = 3
        groupFourParticpantCount = 4
        groupFourSetSize = 1

        self.assertEqual(len(groupsOfAnsweredYes), expectedGroupCount)
        self.assertEqual(groupsOfAnsweredYes[1].getParticipantCount(), groupTwoParticpantCount)
        self.assertEqual(groupsOfAnsweredYes[3].getParticipantCount(), groupFourParticpantCount)
        self.assertEqual(len(groupsOfAnsweredYes[3].getAnswers()), groupFourSetSize)

    def test_getSum(self):
        responseGroup = getGroups(['xxx']) # 3 responses

        expectedGroupCount = 1
        expectedSum = 1

        self.assertEqual(len(responseGroup), expectedGroupCount)

        sum = getSumOfGroups(responseGroup)
        self.assertEqual(sum, expectedSum)

    def test_getSum_multipleAnswers(self):
        responseGroup = getGroups(['a','b','c']) # 3 responses

        expectedGroupCount = 1
        expectedSum = 3

        self.assertEqual(len(responseGroup), expectedGroupCount)

        sum = getSumOfGroups(responseGroup)
        self.assertEqual(sum, expectedSum)

    def test_getSum_ab_ac(self):
        responseGroup = getGroups(['ab','ac']) # 2 responses

        expectedGroupCount = 1
        expectedSum = 3

        self.assertEqual(len(responseGroup), expectedGroupCount)

        sum = getSumOfGroups(responseGroup)
        self.assertEqual(sum, expectedSum)

    def test_consensus_summation(self):
        self.logger.error("")

        tests = [
            TestRun(1, ['a','a','a']),
            TestRun(1, ['a'], "1 Response"),
            TestRun(2, ['abc', 'ac'], "2 participants, 3 answers, [a] (1 consensus for all)"),
            # Test cases in example
            TestRun(3, ['abc']),
            TestRun(0, ['a', 'b', 'c']),
            TestRun(1, ['ab','ac']),
            TestRun(1, ['a','a','a','a']),
            TestRun(1, ['b'])
        ]

        # Run each of the test runs
        for itr, test in enumerate(tests):
            self.logger.error("Run {0} -- Expecting {1} with input [{2}]".format(itr, test.getExpected(), test.getAnswerGroup()))
            responseGroup = getGroups(test.getAnswerGroup())
            sum = getSumConcensusAnswers(responseGroup)
            self.assertEqual(sum, test.getExpected(), test.getMessage())

    def test_consensus_summation_multiple_groups(self):
        self.logger.error("")

        tests = [
            # Test cases in example
            TestRun(6, ['abc','\n','a', 'b', 'c','\n','ab','ac','\n','a','a','a','a','\n','b']),
        ]

        # Run each of the test runs
        for itr, test in enumerate(tests):
            self.logger.error("Run {0} -- Expecting {1}".format(itr, test.getExpected()))
            responseGroup = getGroups(test.getAnswerGroup())
            sum = getSumConcensusAnswers(responseGroup)
            self.assertEqual(sum, test.getExpected(), test.getMessage())


    def test_consensus_from_group_with_two_consensus_answers(self):
        responseGroup = getGroups(['abcdef', 'abf', 'acf', 'adf', 'aef']) # 5 participants, 5 answers, [a,f] (2 consensus for all)

        sum = getSumConcensusAnswers(responseGroup)
        expectedSum = 2

        self.assertEqual(sum, expectedSum)
