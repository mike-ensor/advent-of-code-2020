# import re

from group import Group

valid_passports = 0

def getLines(filename):
    file = open(filename, 'r')
    lines = file.readlines()
    file.close()
    return lines

# Takes all of the lines and gives back groups (array of lines, each line represents 1 person)
def getGroups(lines):
    groups = []
    answers = []

    # Begin parsing Passwords
    for i, fileLine in enumerate(lines):
        line = fileLine.strip()

        if(i+1 == len(lines) and line != ""):
            answers.append(line)
            groups.append(Group(answers))
        elif(line != "" ): # or last line
            answers.append(line)
        else:
            g = Group(answers)
            groups.append(g)
            answers = [] # reset back to empty

    return groups

def getSumOfGroups(groups):
    sum = 0
    # for each group
    for group in groups:
        # for each person in a group, each entry in group set, does the member have
        groupSet = group.getAnswers()
        sum += len(groupSet)
    return sum

def getSumOfGroupsWithAllYes(groups):
    sum = 0
    # for each group
    for group in groups:
        # for each person in a group, each entry in group set, does the member have
        groupSet = group.getAnswers()
        sum += len(groupSet)
    return sum

def getSumConcensusAnswers(groups):
    sum = 0

    for group in groups:
        if(group.getParticipantCount()==1):
            sum += group.getCountAnswers()
        else:
            localSum = 0
            allAnswersForGroup = group.getAnswers()

            for answer in allAnswersForGroup:
                answeredByAll = True # default to true for all participants having answer
                for person in group.getParticipants():

                    intersect = person.getAnswers().intersection(answer)

                    if(len(intersect)==0):
                        answeredByAll = False
                        break
                if answeredByAll == True:
                    localSum += 1
            sum += localSum

    return sum

if __name__ == '__main__':
    groups = getGroups(getLines("input.file"))

    sum = getSumOfGroups(groups)

    print("Sum of YES answers {0}".format(sum))

    sumConsensusAnswers = getSumConcensusAnswers(groups)

    print("Sum of Consensus YES answers {0}".format(sumConsensusAnswers))
