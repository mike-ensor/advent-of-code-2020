

# Person contains a single response, contains their answers and a set of their answers
class Person:
    def __init__(self, answer):
        self.answer = answer
        self.collection = self.__getSetOfAnswers(answer)

    def getAnswers(self):
        return self.collection

    def __getSetOfAnswers(self, answer):
        collection = set()
        for _, v in enumerate(answer):
            collection.add(v)
        return collection


class Group:
    # Constructor
    def __init__(self, answers_by_group):
        self.participants = []
        self.answers_by_group = answers_by_group
        self.collection = self.__getSetOfAllAnswers()

    def getParticipantCount(self):
        return len(self.participants)

    def getParticipants(self):
        return self.participants

    def getAnswers(self):
        return self.collection

    def getPersonAnswers(self, index):
        return self.collection[index].getAnswers()

    def getCountAnswers(self):
        return len(self.collection)

    def getAllParticipantsAnswers(self):
        answers = []
        for participant in self.participants:
            answers.append(participant.getAnswers())
        return answers

    # Returns the set of ALL yes answers
    def __getSetOfAllAnswers(self):
        collection = set()
        for answer in self.answers_by_group:
            p = Person(answer)
            self.participants.append(p)
            # create a union of sets
            collection = collection.union(p.getAnswers())

        return collection
