package main

import (
	"testing"
)

/*
BFFFBBFRRR: row 70, column 7, seat ID 567.
FFFBBBFRRR: row 14, column 7, seat ID 119.
BBFFBBFRLL: row 102, column 4, seat ID 820.
FBFBFFBRRR
*/

func TestAddToSeatMap(t *testing.T) {
	type test struct {
		str    string
		row    int
		seat   int
		seatID int
	}

	// adjust for 0 index
	tests := []test{
		{str: "BFFFBBFRRR", row: 70, seat: 7, seatID: 567},
		{str: "FFFBBBFRRR", row: 14, seat: 7, seatID: 119},
		{str: "BBFFBBFRLL", row: 102, seat: 4, seatID: 820},
		{str: "FBFBFFBRRR", row: 41, seat: 7, seatID: 335},
	}

	for iter, tc := range tests {
		t.Logf("RUN: %d, STR: %s= Row: %d, Seat: %d", iter, tc.str, tc.row, tc.seat)

		planeSeat := GetSeatCoords(tc.str)

		if planeSeat.row != tc.row || planeSeat.seat != tc.seat || planeSeat.id != tc.seatID {
			t.Fatalf("Expected:{Row %d, Seat %d, ID %d} <!==!> Actual:{Row %d, Seat %d, ID %d}", tc.row, tc.seat, tc.seatID, planeSeat.row, planeSeat.seat, planeSeat.id)
		}
	}
}

func TestGetSeatID(t *testing.T) {
	type test struct {
		row      int
		seat     int
		expected int
	}

	// 44 * 8 + 5 = 357
	tests := []test{
		{row: 44, seat: 5, expected: 357},
		{row: 44, seat: 5, expected: 357},
	}

	for iter, tc := range tests {
		t.Logf("RUN: %d, Seat: %d, Row:%d expecting %d", iter, tc.seat, tc.row, tc.expected)
		expected := getSeatID(tc.row, tc.seat)
		if expected != tc.expected {
			t.Fatalf("SeatID %d did not match expected seatID %d", expected, tc.expected)
		}
	}
}

func TestPrintPlane(t *testing.T) {
	seatmap := initializeSeatMap(5, 8)

	// occupy a seat (5th row, 5th seat...remember 0-based array)
	seatmap[4][4] = 1

	// occupy a seat (3rd row, 3rd seat)
	seatmap[2][2] = 1

	str := GetPlaneSeatingPlan(seatmap)

	if str == "" {
		t.Fatal("Seatmap did not return a string")
	}
}

func TestGetRow(t *testing.T) {
	type test struct {
		str      string
		expected int
		size     int
	}

	// adjust for 0 index
	tests := []test{
		{str: "B", expected: 1, size: 1},
		{str: "FFFBBBF", expected: 14, size: MaxRowCount},
		{str: "BBFFBBF", expected: 102, size: MaxRowCount},
		{str: "BFFFBBF", expected: 70, size: MaxRowCount},
		{str: "FBFBBFF", expected: 44, size: MaxRowCount},
		{str: "B", expected: 1, size: 1},
		{str: "F", expected: 0, size: 1},
		{str: "FB", expected: 2, size: 4},
		{str: "FF", expected: 0, size: 4},
		{str: "BF", expected: 2, size: 4},
		{str: "BB", expected: 4, size: 4},
		{str: "FFF", expected: 0, size: 6},
		{str: "FBF", expected: 2, size: 6},
		{str: "FBB", expected: 3, size: 6},
	}

	for iter, tc := range tests {
		t.Logf("RUN: %d, STR: %s => [%d]", iter, tc.str, tc.expected)
		row := getPositionByInput(tc.str, 0, tc.size)
		if row != tc.expected { // +1 for "zero index"
			t.Fatalf("STR[%s], Row %d did not match expected row %d", tc.str, row, tc.expected)
		}
	}
}

func TestGetSeatAssignment(t *testing.T) {
	type test struct {
		str      string
		iBottom  int
		iTop     int
		expected int
	}

	// adjust for 0 index
	tests := []test{
		{str: "RLR", iBottom: 0, iTop: MaxColumnWidth, expected: 5},
		{str: "RRR", iBottom: 0, iTop: MaxColumnWidth, expected: 7},
		{str: "RRL", iBottom: 0, iTop: MaxColumnWidth, expected: 6},
		{str: "RLR", iBottom: 0, iTop: MaxColumnWidth, expected: 5},
		{str: "RLL", iBottom: 0, iTop: MaxColumnWidth, expected: 4},
		{str: "LRR", iBottom: 0, iTop: MaxColumnWidth, expected: 3},
		{str: "LRL", iBottom: 0, iTop: MaxColumnWidth, expected: 2},
		{str: "LLR", iBottom: 0, iTop: MaxColumnWidth, expected: 1},
		{str: "LLL", iBottom: 0, iTop: MaxColumnWidth, expected: 0},
	}

	for iter, tc := range tests {
		t.Logf("RUN: %d, STR: %s => [%d]", iter, tc.str, tc.expected)
		seat := getPositionByInput(tc.str, tc.iBottom, tc.iTop)
		if seat != tc.expected { // +1 for "zero index"
			t.Fatalf("Str: %s, Actual %d != Expected %d", tc.str, seat, tc.expected)
		}
	}
}

func TestGetNewSplit(t *testing.T) {
	type test struct {
		iBottom int
		iTop    int
		eBottom int
		eTop    int
		str     string
	}

	// adjust for 0 index
	tests := []test{
		{iBottom: 8, iTop: 10, eBottom: 9, eTop: 10, str: "B"},
		{iBottom: 0, iTop: 10, eBottom: 0, eTop: 5, str: "F"},
		{iBottom: 0, iTop: 10, eBottom: 5, eTop: 10, str: "B"},
		{iBottom: 8, iTop: 10, eBottom: 9, eTop: 10, str: "B"},
		{iBottom: 8, iTop: 10, eBottom: 8, eTop: 9, str: "F"},
		{iBottom: 10, iTop: 10, eBottom: 10, eTop: 10, str: "F"},
		{iBottom: 10, iTop: 10, eBottom: 10, eTop: 10, str: "B"},
		{iBottom: 0, iTop: 8, eBottom: 4, eTop: 8, str: "R"},
		{iBottom: 0, iTop: 8, eBottom: 0, eTop: 4, str: "L"},
	}

	for iter, tc := range tests {
		t.Logf("RUN: %d, STR: %s", iter, tc.str)
		actualBottom, actualTop := getNewSplit(tc.str, tc.iBottom, tc.iTop)
		if tc.eBottom != actualBottom {
			t.Fatalf("Str: %s, Bottom Bound Actual [%d] != Expected [%d]", tc.str, actualBottom, tc.eBottom)
		}
		if tc.eTop != actualTop {
			t.Fatalf("Str: %s, Top Bound Actual [%d] != Expected [%d]", tc.str, actualTop, tc.eTop)
		}
	}
}
