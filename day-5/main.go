package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"strings"
)

// PlaneSeat holds a single seat for a plane ride
type PlaneSeat struct {
	row  int
	seat int
	id   int
}

// MaxColumnWidth are the number of seats in a row
var MaxColumnWidth int = 7

// MaxRowCount is the number of rows from front to back
var MaxRowCount int = 127

func main() {
	fmt.Print("\nFinding sum of numbers: \n...\n...\n")
	input := loadInput("input.file")

	seatmap := Run(input)

	fmt.Println(GetPlaneSeatingPlan(seatmap))
}

// Run is the primary method for testing purposes
func Run(input []string) [][]int {

	maxID := 0

	seatmap := initializeSeatMap(MaxRowCount+1, MaxColumnWidth+1)

	for iter, inputValue := range input {
		fmt.Printf("%d, %s\n", iter, inputValue)
		planeSeat := GetSeatCoords(inputValue)
		seatmap[planeSeat.row][planeSeat.seat] = planeSeat.id
		if planeSeat.id > maxID {
			maxID = planeSeat.id
		}
	}

	fmt.Printf("\n\n----------\n MAX: %d\n\n", maxID)

	return seatmap
}

// GetSeatCoords takes in a seatmap and an assignment, then returns an updated seatmap
func GetSeatCoords(seatString string) PlaneSeat {

	// magic numbers
	firstSeven := seatString[0:7]
	lastThree := seatString[7:]
	log.Printf("[%s:%s]\n", firstSeven, lastThree)

	row := getPositionByInput(firstSeven, 0, MaxRowCount)    // zero index
	seat := getPositionByInput(lastThree, 0, MaxColumnWidth) // zero index

	seatCoord := PlaneSeat{row: row, seat: seat, id: getSeatID(row, seat)}
	printSeat(seatCoord)

	return seatCoord
}

func printSeat(seat PlaneSeat) {
	fmt.Printf("> R: %d, S: %d, ID: %d", seat.row, seat.seat, seat.id)
}

// GetPlaneSeatingPlan prints to standard out the current plane seating
func GetPlaneSeatingPlan(seatmap [][]int) string {
	var buffer bytes.Buffer
	buffer.WriteString("\n")

	empty := "."
	taken := "X"

	columns := len(seatmap[0])
	buffer.WriteString(fmt.Sprintf("%-10s", " ")) // 10 characters to cover for "Row %-3d"
	for column := 0; column < columns; column++ {
		buffer.WriteString(fmt.Sprintf(" %d ", column+1))
	}
	buffer.WriteString(fmt.Sprintf("\n%s\n", strings.Repeat("=", (columns*3)+10)))

	for iter := range seatmap {
		buffer.WriteString(fmt.Sprintf("Row %-3d = ", iter+1))
		for _, seat := range seatmap[iter] {
			if seat > 0 {
				buffer.WriteString(fmt.Sprintf(" %s ", taken))
			} else {
				buffer.WriteString(fmt.Sprintf(" %s ", empty))
			}
		}
		buffer.WriteString(fmt.Sprintf("\n"))
	}

	return buffer.String()
}

func getPositionByInput(digits string, bottom int, top int) int {
	letter := string(digits[0])

	newBottom, newTop := getNewSplit(letter, bottom, top)
	next := digits[1:]

	if next == "" {
		if letter == "F" || letter == "L" {
			// Front Half
			return newBottom
		} else if letter == "B" || letter == "R" {
			// Back Half
			return newTop
		}
	}

	return getPositionByInput(next, newBottom, newTop)
}

func getNewSplit(letter string, bottom int, top int) (int, int) {
	// if F, use lower half, if B keep upper half
	newBottom := bottom
	newTop := top
	pointer := ((top - bottom) / 2) + bottom

	if letter == "F" || letter == "L" {
		// Front Half
		newTop = pointer
	} else if letter == "B" || letter == "R" {
		// Back Half
		if (top-bottom)%2 == 1 {
			// non-even splits need to go to the right by 1
			pointer = pointer + 1
		}

		newBottom = pointer
	}

	return newBottom, newTop
}

// loadInput returns an array of each line in the file as a String value
func loadInput(fileName string) []string {

	file, err := os.Open(fileName)
	defer file.Close()

	if err != nil {
		log.Fatalf("failed to open %v", fileName)
	}

	scanner := bufio.NewScanner(file)

	var values []string

	for scanner.Scan() {
		values = append(values, scanner.Text())
	}

	return values
}

func initializeSeatMap(row int, column int) [][]int {
	// SeatMap is the current seating map
	seatmap := make([][]int, row)

	for i := range seatmap {
		seatmap[i] = make([]int, column)
	}

	return seatmap
}

func getSeatID(row int, seat int) int {
	return (row * 8) + seat
}
