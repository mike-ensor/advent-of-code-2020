package main

import (
	"testing"
)

func TestSumFound(t *testing.T) {
	input := []int{10, 2, 3, 7, 5}
	target := 12

	first, second, third := FindSumOfThree(target, input)
	sum := first + second + third

	if sum != target {
		t.Errorf("Returned values (%d, %d and %d) do not sum to target %d", first, second, third, target)
	}
}

func TestSumNotFound(t *testing.T) {
	input := []int{10, 2, 3, 7, 5}
	target := 21

	first, second, third := FindSumOfThree(target, input)
	sum := first + second + third

	if sum == target {
		t.Errorf("Returned values (%d, %d and %d) DO sum to target %d  and shouldn't", first, second, third, target)
	}
}
