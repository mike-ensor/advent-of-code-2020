package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	fmt.Print("Finding sum of numbers\n...\n...\n")
	input := loadInput("input.file")
	target := 2020

	first, second, third := FindSumOfThree(target, input)
	sum := first + second + third

	if sum == target {
		answer := first * second * third
		fmt.Printf("Found: %d + %d + %d = %d\n============================\n", first, second, third, sum)
		fmt.Printf("Product result (and answer): %d\n============================\n", answer)
	} else {
		log.Fatalf("No sum of two integers were found to match '%d', therefore there is NO result answer", target)
	}

}

// FindSumOfThree returns three numbers that are a sum of three numbers given by the input array
func FindSumOfThree(target int, input []int) (int, int, int) {

	first := 0
	second := 0
	third := 0
	found := false

	firstEnd := len(input)-3 // can't be larger than 3 (3 points)

	for _firstPtr := 0; _firstPtr < firstEnd; _firstPtr++ {
		secondStart := _firstPtr + 1
		secondEnd := len(input)

		if found {
			break
		}

		for _secondPtr := secondStart; _secondPtr < secondEnd; _secondPtr++ {
			if found {
				break
			}

			thirdStart := _secondPtr +1
			thirdEnd := len(input)

			for _thirdPtr := thirdStart; _thirdPtr < thirdEnd; _thirdPtr++ {

				checkLeft := input[_firstPtr]
				checkRight := input[_secondPtr]
				checkLast := input[_thirdPtr]

				sum := checkLeft + checkRight + checkLast

				if sum == target {
					first = checkLeft
					second = checkRight
					third = checkLast
					found = true
					break
				}
			}

		}
	}

	return first, second, third
}

func loadInput(fileName string) []int {

	file, err := os.Open(fileName)
	defer file.Close()

	if err != nil {
		log.Fatalf("failed to open %v", fileName)
	}

	scanner := bufio.NewScanner(file)

	var values []int

	for scanner.Scan() {
		intVal, _ := strconv.Atoi(scanner.Text())
		values = append(values, intVal)
	}

	return values
}
