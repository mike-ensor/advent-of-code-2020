# Overview

>NOTE: [Original Link - Part 2](https://adventofcode.com/2020/day/1#part2)

## tl;dr
1. Extend Part 1 and find three entries that sum to `2020`
1. Multiple those three entries for solution

