# Overview

>NOTE: [Original Link](https://adventofcode.com/2020/day/1)

## tl;dr
1. Find two entries that sum to `2020`
1. Multiple those two entries for solution

