package main

import (
	"testing"
)

func TestSumFound(t *testing.T) {
	input := []int{10, 2, 3, 7, 5}
	target := 5

	left, right := FindSum(target, input)
	sum := left + right

	if sum != target {
		t.Errorf("Returned values (%d and %d) do not sum to target %d", left, right, target)
	}
}

func TestSumNotFound(t *testing.T) {
	input := []int{10, 2, 3, 7, 5}
	target := 20

	left, right := FindSum(target, input)
	sum := left + right

	if sum == target {
		t.Errorf("Returned values (%d and %d) DO sum to target %d  and shouldn't", left, right, target)
	}
}
