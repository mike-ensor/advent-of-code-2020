package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	fmt.Print("\nFinding sum of numbers: \n...\n...\n")
	input := loadInput("input.file")
	target := 2020

	left, right := FindSum(target, input)
	sum := left + right

	if sum == target {
		answer := left * right
		fmt.Printf("Found: %d + %d = %d\n============================\n", left, right, sum)
		fmt.Printf("Product result (and answer): %d\n============================\n", answer)
	} else {
		log.Fatalf("No sum of two integers were found to match '%d', therefore there is NO result answer", target)
	}

}

// FindSum returns two numbers that are a sum of the given input number
func FindSum(target int, input []int) (int, int) {
	// initial thought (simple solution, not efficient solution)
	// double loop, pointer + pointer+i == target (stop)
	left := 0
	right := 0

	for pointer := 0; pointer < len(input); pointer++ {
		start := pointer + 1
		end := len(input) - pointer
		for look := start; look < end; look++ {
			checkLeft := input[pointer]
			checkRight := input[look]
			sum := checkLeft + checkRight

			if sum == target {
				left = checkLeft
				right = checkRight
				break
			}
		}
	}

	return left, right
}

func loadInput(fileName string) []int {

	file, err := os.Open(fileName)
	defer file.Close()

	if err != nil {
		log.Fatalf("failed to open %v", fileName)
	}

	scanner := bufio.NewScanner(file)

	var values []int

	for scanner.Scan() {
		intVal, _ := strconv.Atoi(scanner.Text())
		values = append(values, intVal)
	}

	return values
}
