package main

import (
	"testing"
)

func TestGetRuleTargetName(t *testing.T) {
	type test struct {
		str      string
		expected string
	}

	tests := []test{
		{str: "bright gold bags contain 5 mirrored beige bags, 1 bright coral bag, 2 plaid turquoise bags, 3 striped crimson bags.", expected: "bright gold"},
		{str: "mirrored gold bags contain no other bags.", expected: "mirrored gold"},
	}

	for iter, tc := range tests {
		t.Logf("RUN: %d, STR: %s", iter, tc.str)

		result := getName(tc.str)

		if tc.expected != result {
			t.Fatalf("Target name was not what was expected")
		}
	}
}

func TestGetChildRuleTargetName(t *testing.T) {

	str := "bright gold bags contain 5 mirrored beige bags, 1 bright coral bag, 2 plaid turquoise bags, 3 striped crimson bags."
	expected := []string{"mirrored beige", "bright coral", "plaid turquoise", "striped crimson"}
	expectedCount := []int{5, 1, 2, 3}

	result := getChildBags(str)

	for iter := range expected {
		// FIXME Totally dependent on position in the result
		if expected[iter] != result[iter].name {
			t.Fatalf("Target name was not what was expected")
		}
		if expectedCount[iter] != result[iter].max {
			t.Fatalf("Target name was not what was expected")
		}
	}
}

func TestGetChildRuleTargetName_NoOtherBags(t *testing.T) {

	str := "mirrored gold bags contain no other bags"

	result := getChildBags(str)

	if len(result) > 0 {
		t.Fatalf("There should be no results")
	}

}

/*
A bright white bag, which can hold your shiny gold bag directly.
A muted yellow bag, which can hold your shiny gold bag directly, plus some other bags.
A dark orange bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
A light red bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
*/