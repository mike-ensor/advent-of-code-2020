package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

// ChildBagRule contains information captured by a rule
type ChildBagRule struct {
	name string
	max  int
}

// Rule contains the requirement for a rule
type Rule struct {
	name     string
	children []ChildBagRule
}

func main() {
	input := loadInput("input.file")
	setupRules(input)
}

func setupRules(rules []string) {

	for iter, inputValue := range rules {
		name := getName(inputValue)
		children := getChildBags(inputValue)

		fmt.Printf("%d - Name: %s, %d children - %s\n", iter, name, len(children), inputValue)
	}

}

func getName(input string) string {
	target := regexp.MustCompile(`(?P<name>\w+\s\w+)\sbags contain\s`)

	out := target.FindStringSubmatch(input)

	// TODO: Add a check here for "is valid match"
	return out[1]
}

func getChildBags(input string) []ChildBagRule { // TODO: Set this up as a Struct
	target := regexp.MustCompile(`(?P<amount>\d+)\s*(?P<name>\w+\s*\w*)\s+bag[s]*`)
	var retVal []ChildBagRule

	out := target.FindAllStringSubmatch(input, -1)

	for _, result := range out {

		count, _ := strconv.Atoi(result[1])

		childRule := ChildBagRule{
			name: result[2],
			max:  count,
		}

		retVal = append(retVal, childRule)
	}

	return retVal
}

// loadInput returns an array of each line in the file as a String value
func loadInput(fileName string) []string {

	file, err := os.Open(fileName)
	defer file.Close()

	if err != nil {
		log.Fatalf("failed to open %v", fileName)
	}

	scanner := bufio.NewScanner(file)

	var values []string

	for scanner.Scan() {
		values = append(values, scanner.Text())
	}

	return values
}
