# Approach

1. Load line-by-line
1. For each line, use regex groups to pull out each of the important data
1. Regex: `^(\d+)-(\d+)\s([a-z])[:]\s([a-z]+)$`
1. If the `{1}` <= `{4}.count({3})` <= `{2}` (lowest number <= count-of-character-in-password <= highest number), then add to counter
1. Print out final count
