import re

# main
# 1. For each line in file
  # Get rule (##-##) [a-z]: <password>
  # example: 13-14 f: ffffffffnfffvv
  # Verify rule (count letter in string) - count valid

correct_passwords = 0
pattern = re.compile("^(\d+)-(\d+)\s([a-z])[:]\s([a-z]+)$")

valid_passwords = 0
totalCount = 0

# open file
file = open('input.file', 'r')
# Collect all lines
Lines = file.readlines()

# Lines = """
# 1-3 a: abcde
# 1-3 b: cdefg
# 2-9 c: ccccccccc
# """.split("\n")

# Begin parsing passwords
for fileLine in Lines:
    if( fileLine == ""):
        continue
    line = fileLine.strip()
    totalCount = totalCount+1

    # match pattern
    m = pattern.match(line)

    if m:
        first = int(m.group(1))
        second = int(m.group(2))
        letter = m.group(3)
        password = m.group(4)

        if password[first-1] == letter and password[second-1] != letter:
            print("{0}-{1} {2}: {3}".format(first, second, letter, password))
            valid_passwords=valid_passwords+1
        elif password[second-1] == letter and password[first-1] != letter:
            print("{0}-{1} {2}: {3}".format(first, second, letter, password))
            valid_passwords=valid_passwords+1


print("# of valid passwords = {0} out of {1}".format(valid_passwords, totalCount))

