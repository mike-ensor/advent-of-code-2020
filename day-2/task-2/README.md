# Approach

## Difference from Task 1

1. First number = character must match the 'character' in that position
1. Second number = character must NOT match the 'character' in that position

## Overview

1. Approach is similar, use the regex to capture the important details
1. Use "char at" function on the password to validate `{1}` == `{4}.charAt({1})` && `{2}` != `{4}.charAt({2})`
1. If true, then add to count

# Thoughts

I first read the problem as the 1st number needs to match the target character at that position and the second cannot match the character. The opposite is also true, the second or first number at their respective positions must match the target, but not both

