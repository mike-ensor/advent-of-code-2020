package main

import (
	"reflect"
	"testing"
)

// MoveDistance how many spaces to move to the right
var MoveDistance int = 3

func TestGetTreesHit_OnIndex(t *testing.T) {
	pattern := [3]string{
		".#.#....##.......#..........#..",
		"..#..#.........##...#..#.......", /* move three right, down 1...the down is on a tree */
		"#.####......##.#...#......#.#..",
	}

	expected := 1
	line := 2

	result := GetTreesHit(pattern[line], 2+MoveDistance)

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestGetTreesHit_FirstIndex(t *testing.T) {
	pattern := [3]string{
		".#.#....##.......#..........#..",
		"..#..#.........##...#..#.......", /* move three right, down 1...the down is on a tree */
		"#.####......##.#...#......#.#..",
	}

	expected := 1
	line := 1

	result := GetTreesHit(pattern[line], MoveDistance)

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestGoDownHill_FirstRow(t *testing.T) {
	pattern := [2]string{
		".#.#....##.......#..........#..",
		"...............................",
	}
	expected := 0

	result := GoDownHill(pattern[:], 1, 3)

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestGoDownHill_TwoRows(t *testing.T) {
	pattern := [2]string{
		"..#.#....##.......#..........#..",
		"...#..#.........##...#..#.......",
	}

	expected := 1

	result := GoDownHill(pattern[:], 1, 3)

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestGoDownHill_ThreeRows(t *testing.T) {
	pattern := [3]string{
		"..#.#....##.......#..........#..",
		"...#..#.........##...#..#.......",
		".#.####......##.#...#......#.#..",
	}

	expected := 2

	result := GoDownHill(pattern[:], 1, 3)

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestGoDownHill_WrappingTwice(t *testing.T) {
	pattern := [4]string{
		".#.#...",
		"..##.#.", /* 3rd from left */
		"......#", /* All of these, but have to extend */
		"..#....",
	}

	expected := 3

	result := GoDownHill(pattern[:], 1, 3)

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestRunWithReal(t *testing.T) {

	lines := loadInput("test-input/test.input")
	result := GoDownHill(lines, 1, 3)

	expected := 7

	if result != expected {
		t.Errorf("Expected %d trees hit, found %d", expected, result)
	}
}

func TestRunWithReal_DifferentStrategies(t *testing.T) {
	type test struct {
		right int
		down  int
		want  int
	}

	tests := []test{
		{right: 1, down: 2, want: 2},
		{right: 1, down: 1, want: 2},
		{right: 3, down: 1, want: 7},
		{right: 5, down: 1, want: 3},
		{right: 7, down: 1, want: 4},
	}

	lines := loadInput("test-input/test.input")
	for iter, tc := range tests {
		got := GoDownHill(lines, tc.down, tc.right)
		if !reflect.DeepEqual(tc.want, got) {
			t.Fatalf("iteration: %d:  expected: %v, got: %v", iter, tc.want, got)
		}
	}
}
