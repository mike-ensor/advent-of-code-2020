package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// Debug for extra output
var Debug bool = false

// TreeCharacter character representing a tree
var TreeCharacter string = "#"

// Strategy holds a strategy and result
type Strategy struct {
	right  int
	down   int
	result int
}

func main() {
	// lines := loadInput("test-input/test.input") // Testing
	lines := loadInput("input.file") // Normal
	// Right 1, down 1. = 2
	// Right 3, down 1. = 7
	// Right 5, down 1. = 3
	// Right 7, down 1. = 4 X
	// Right 1, down 2. = 2
	runs := []Strategy{
		{right: 1, down: 1},
		{right: 3, down: 1},
		{right: 5, down: 1},
		{right: 7, down: 1},
		{right: 1, down: 2},
	}

	sum := 1

	for iter, run := range runs {
		result := GoDownHill(lines, run.down, run.right)
		sum *= result
		fmt.Printf("Run: %d: Right(%d) Down(%d) = %d\n", iter+1, run.right, run.down, result)
	}

	fmt.Printf("\n==================\nSum: %d\n\n", sum)

}

// GoDownHill is the main game engine
func GoDownHill(lines []string, down int, right int) int {

	pointer := 1  // Start at move spaces and on second line
	treesHit := 0 // initialize to zero trees hit

	if Debug == true {
		fmt.Printf("RIGHT: %d, DOWN: %d\n", right, down)
	}

	for index, pattern := range lines {

		skipLines := index % down

		if index == 0 {
			pointer += right
			if Debug == true {
				fmt.Println(pattern)
			}
			continue
		}
		if skipLines == 0 {
			courseLine := pattern

			for len(courseLine) < pointer+right {
				courseLine += courseLine
			}

			treesHit += GetTreesHit(courseLine, pointer)

			pointer += right
		}
	}

	return treesHit
}

// GetTreesHit counts the number of trees are hit when moving across
func GetTreesHit(pattern string, index int) int {
	trees := 0

	moveIndex := index // will be at the "down"
	stoppingCharacter := string(pattern[moveIndex-1])
	if stoppingCharacter == TreeCharacter {
		trees++
	}

	if Debug {
		hitChar := 'O'
		if trees > 0 {
			hitChar = 'X'
		}
		debugLine := replaceAtIndex(pattern, hitChar, moveIndex-1)
		fmt.Println(debugLine)
	}

	return trees
}

func loadInput(fileName string) []string {

	file, err := os.Open(fileName)
	defer file.Close()

	if err != nil {
		log.Fatalf("failed to open %v", fileName)
	}

	scanner := bufio.NewScanner(file)

	var values []string

	for scanner.Scan() {
		values = append(values, scanner.Text())
	}

	return values
}

func replaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}
