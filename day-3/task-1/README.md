# Overview

First approach will be row-by-row rather than some other approach like an algorithm :)

>NOTE: Only go over 3, down 1...IF there is a # then it's a hit (it's on the "down", not on the "move right")

1. Establish a "move-right" counter (n)
1. Establish a pointer representing the current from-left column (always adding 3 or n)
1. Move:
    1. View row + move-right counter IF the end is a "#" (tree)
    1. If current pointer is greater than length of string, add the same string to the end and proceed
    1. Count the # of trees in (n) steps to right
    1. Record new index position
    1. Repeat

## Observations

This will not be easy, it'll involve a TON of duplicating of strings to start with


