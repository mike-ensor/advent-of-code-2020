import re

from passport import Passport

valid_passports = 0

def getLines(filename):
    file = open(filename, 'r')
    lines = file.readlines()
    file.close()
    return lines

def getFullPassportLines(lines):
    passportLine = ""
    passportLines = []
    # Begin parsing Passwords
    for fileLine in lines:

        line = fileLine.strip()

        if(line == "" ):
            passportLines.append(passportLine)
            passportLine = "" # Reset
        else:
            trimmedString = passportLine + " " + line
            passportLine = trimmedString.strip()

    if(passportLine != ""):
        passportLines.append(passportLine)

    return passportLines

def getValidPassports(lines):
    validPassports = []
    for line in lines:
        p = Passport(line)
        if(p.isValid()):
            validPassports.append(p)
    return validPassports


if __name__ == '__main__':
    lines = getFullPassportLines(getLines("input.file"))

    validPassports = getValidPassports(lines)

    print("Valid passports {0}".format(len(validPassports)))