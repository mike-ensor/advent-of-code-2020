import unittest

from passport import Passport

class PassportTestCase(unittest.TestCase):

    def test_validPassport_allEight(self):
       line = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), True)

    def test_validPassport_ProvidedValid(self):
       line = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f"
       p = Passport(line)
       self.assertEqual(p.isValid(), True)

    def test_inValidPassport_allEight(self):
       # Invalid eye Color
       line = "ecl:gry pid:860033327 eyr:2019 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)

    def test_inValidPassport_AnotherFromTheList(self):
       # Invalid eye Color
       line = "hcl:dab227 iyr:2012 ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)

    def test_inValidPassport_fromList(self):
       line = "eyr:1972 cid:100 hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)

    def test_validPassport_allEightButNotValidField(self):
       line = "ecl:gry pid:860033327 eyr:2020 XXX:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)

    def test_validPassport_allEightButNoCID_andOneInValidField(self):
       line = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 XXX:147 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)

    def test_validPassport_lessThanSeven(self):
       line = "ecl:gry pid:860033327 byr:1937 iyr:2017 cid:147 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)

    def test_validPassport_sevenWithoutCid(self):
       line = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), True)

    def test_validPassport_sevenWithCid(self):
       line = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd cid:147 iyr:2017 hgt:183cm"
       p = Passport(line)
       self.assertEqual(p.isValid(), False)
