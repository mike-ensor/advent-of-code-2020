import unittest

from rules import *

class RulesTestCase(unittest.TestCase):

    def test_validRange(self):
        rule = RangeRule(0,10)
        self.assertEqual(rule.isValid(5), True)

    def test_invalidRange(self):
        rule = RangeRule(0,10)
        self.assertEqual(rule.isValid(-1), False)

    def test_validLowerAndHigher(self):
        rule = RangeRule(5,5)
        self.assertEqual(rule.isValid(5), True)

    def test_height(self):
        rule = HightRule(59,70,150,193)
        self.assertEqual(rule.isValid("60in"), True)
        self.assertEqual(rule.isValid("59in"), True)
        self.assertEqual(rule.isValid("58in"), False)
        self.assertEqual(rule.isValid("70in"), True)
        self.assertEqual(rule.isValid("71in"), False)
        self.assertEqual(rule.isValid("150cm"), True)
        self.assertEqual(rule.isValid("193cm"), True)
        self.assertEqual(rule.isValid("180cm"), True)
        self.assertEqual(rule.isValid("149cm"), False)
        self.assertEqual(rule.isValid("194cm"), False)

    def test_hairColorRule(self):
        rule = HairColorRule()
        self.assertEqual(rule.isValid("#a123ab"), True)
        self.assertEqual(rule.isValid("a123ab"),  False)
        self.assertEqual(rule.isValid("aa123ab"), False)
        self.assertEqual(rule.isValid("#zzzzzz"), False)

    def test_EyeColor(self):
        rule = EyeColorRule()
        self.assertEqual(rule.isValid("amb"), True)
        self.assertEqual(rule.isValid("xyz"),  False)
        self.assertEqual(rule.isValid("hzl"), True)

    def test_Passport(self):
        rule = PassportRule()
        self.assertEqual(rule.isValid("000000000"), True)
        self.assertEqual(rule.isValid("00000000"),  False)
        self.assertEqual(rule.isValid("999999999"), True)
        self.assertEqual(rule.isValid("a00000000"),  False)
