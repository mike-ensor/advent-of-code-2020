import unittest

from main import *

class MainTestCase(unittest.TestCase):

    def test_main(self):
       result = getFullPassportLines(getLines("simple-test.file"))
       self.assertEqual(len(result), 4)

    def test_getValidPassports_singleLine(self):
        lines = ["ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"]
        passports = getValidPassports(lines)

        self.assertEqual(len(passports), 1)

    def test_getValidPassports_inValid(self):
        lines = ["iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929"]
        passports = getValidPassports(lines)

        self.assertEqual(len(passports), 0)

    def test_getValidPassports_validWithSevenFields(self):
        lines = ["hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm"]
        passports = getValidPassports(lines)

        self.assertEqual(len(passports), 1)

    # Task 2 - Provided Invalid
    def test_getValidPassports_providedInValid(self):
        lines = ["hcl:dab227 iyr:2012 ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277"]
        passports = getValidPassports(lines)

        self.assertEqual(len(passports), 0)

    # Task 2 - Provided Valid
    def test_getValidPassports_providedValid(self):
        lines = ["pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f"]
        passports = getValidPassports(lines)

        self.assertEqual(len(passports), 1)