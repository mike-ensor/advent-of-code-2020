# Approach

## Fields
* byr (Birth Year)
* iyr (Issue Year)
* eyr (Expiration Year)
* hgt (Height)
* hcl (Hair Color)
* ecl (Eye Color)
* pid (Passport ID)
* cid (Country ID) -- OPTIONAL

## Rules
1. All 8 fields must be filled
    >Except: if the the "cid" is empty but all others aren't

## Process
1. Load line-by-line
1. Capture each Passport (new, empty lines is the break between records)
1. For each line, use regex groups to capture
1. If less than 7 fields, not valid
1. else, if all 8 == good, if 7 are filled but not CID, then it's valid
1. else, not valid
1. Print out final count

## Additions

1. Create rules for each field