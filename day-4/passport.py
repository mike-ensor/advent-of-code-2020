import re
from rules import *

class Field:
    def __init__(self, field, rule):
            self.field = field
            self.rule = rule
class Passport:
    pattern = re.compile("\s*([\w]+)[:]([a-z0-9#]+)\s*")
    __approvedFields = {'byr':RangeRule(1920,2002),
                        'iyr':RangeRule(2010, 2020),
                        'eyr':RangeRule(2020, 2030),
                        'hgt':HightRule(59,76,150,193),
                        'hcl':HairColorRule(),
                        'ecl':EyeColorRule(),
                        'pid':PassportRule(),
                        'cid':EmptyRule()
                        }
    __hasInvalidField = False

    # Constructor
    def __init__(self, line):
        self.__dict__["count"] = 0
        for (key, value) in re.findall(self.pattern, line):
            # Don't set attributes if they are not in the approved list
            if key in self.__approvedFields.keys():
                rule = self.__approvedFields[key]
                if rule.isValid(value):
                    self.__dict__[key] = value
                    self.__dict__["count"] += 1
            else:
                self.__hasInvalidField = True

    def __getattr__(self, name):
        found = ""
        if name in self.__dict__.keys():
            found = self.__dict__[name]
        return found

    def isValid(self):

        retValue = False
        if self.count == 8:
            retValue = True
        elif self.count == 7:
            if(self.cid == "" and not self.__hasInvalidField):
                # Only allowed to have 7 IF there is no CID (and all fields are valid)
                retValue = True

        return retValue
