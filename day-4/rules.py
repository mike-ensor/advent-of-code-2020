import re

class RangeRule:
    def __init__(self, low, high):
        self.low = low
        self.high = high
    def isValid(self, value):
        val = int(value)
        return self.low <= val <= self.high

class HairColorRule:
    def isValid(self, value):
        valid = False
        pattern = re.compile("^[#]{1}[a-f0-9]{6}$")
        if re.search(pattern, value):
            valid = True
        return valid

class PassportRule:
    def isValid(self, value):
        valid = False
        pattern = re.compile("^[0-9]{9}$")
        if re.search(pattern, value):
            valid = True
        return valid

class EyeColorRule:
    def __init__(self):
        self.validFields = ['amb','blu','brn','gry','grn','hzl','oth']

    def isValid(self, value):
        valid = False
        if value in self.validFields:
            valid = True
        return valid

class HightRule:
    def __init__(self, low_imp, high_imp, low_met, high_met):
        self.low_imp = low_imp
        self.high_imp = high_imp
        self.low_met = low_met
        self.high_met = high_met

    def isValid(self, value):
        pattern = re.compile("\s*([\d]+)([a-z]+)\s*")
        valid = False

        match = pattern.match(value)

        if match:
            height = match.group(1)
            t = match.group(2)
            if(t == "cm"):
                valid = self.low_met <= int(height) <= self.high_met
            elif(t == "in"):
                valid = self.low_imp <= int(height) <= self.high_imp

        return valid


class EmptyRule:
    def isValid(self, value):
        return True